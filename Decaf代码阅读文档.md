##Decaf代码阅读文档
####Monitor命令:  
**load_plugin [filename]** ---> shared/DECAF_main.c:do_load_plugin() ---> do_load_plugin_internal() ---> init_plugin()、污点等功能的初始配置、“loaded successfully!”
unload_plugin ---> do_load_plugin()：plugin_cleanup()，hookapi_flush_hooks()：清除插件可能注册的所有回调，“is unloaded!”  

**enable_tainting**:打开污点跟踪 ---> shared/tainting/taint_memory.c: do_enable_tainting():”Taint tracking is now enabled” ---> do_enable_tainting_internal():刷新所有的翻译块，申请污点内存页表空间。  

**taint_pointers [on/off] [on/off]**：开关 指针的读或写的污点跟踪 ---> do_taint_pointers():设置值taint_load_pointers_enabled和taint_store_pointers_enabled，刷新所有的翻译块，"Tainting of pointers changed -> Load: xx, Store:xx".  

####callbacktests
**init_plugin()**：注册callbacktests_term_cmds和callbacktests_cleanup俩函数 ---> callbacktests_init()：“Hello World”:  
- 注册回调函数VMI_CREATEPROC_CB ---> callbacktests_loadmainmodule_callback():VMI_find_process_by_cr3_c() ---> runTests()：“Running the test”:  
 - 注册DECAF_BLOCK_BEGIN_CB ---> callbacktests_genericcallback()
 - 注册DECAF_BLOCK_END_CB /
 - 注册DECAF_INSN_BEGIN_CB /
 - 注册DECAF_INSN_END_CB /

- 注册回调函数VMI_REMOVEPROC_CB ---> callbacktests_removeproc_callback():把runTests()中注册的函数注销，并计数统计，“Callback Count”; 如果CALLBACKTESTS_TEST项目全部测试完成，则调用	callbacktests_printSummary()：“SUMMARY”.  

---> do_callbacktests():“测试进程描述” ---> callbacktests_resetTests()：初始化callbacktests结构体。  

**Monitor命令：do_callbacktests [procname]** ---> do_callbacktests()：接收命令参数、“测试进程描述” ---> callbacktests_resetTests()：初始化callbacktests结构体。  

####Hookapitests
DECAF_registerOptimizedBlockBeginCallback(&hookapi_check_hook, NULL, pc, OCB_CONST)为核心函数.  

**init_plugin()**：mon_cmds、plugin_cleanup处理函数注册 ---> hookapitests_init():”Hello World”,
- 注册回调函数VMI_CREATEPROC_CB：createproc_callback():新进程名与targetname相同，则保存进程信息，”Process found: pid=xx, cr3=xx”, 调用 ---> register_hooks():
 - 注册“模块”“函数名”，处理函数NtCreateFile_call：“NtCreateFile entry”，保存栈顶 ---> hookapi_hook_return():
   - 用DECAF_registerOptimizedBlockBeginCallback()注册函数的回调 以检查回调是否为ret函数，处理函数为NtCreateFile_ret():”NtCreateFile exit” ---> hookapi_remove_hook():移除函数的hook ---> hookapi_insert():需要hook的函数信息插入记录链.  

- ---> hookapi_hook_function_byname():通过funcmap_get_pc()获取pc，无则”Deferring hooking”, add_unresolved_hook()记录hook函数及其回调，有则“Hooking %s::%s @ 0x%x”---> hookapi_hook_function()：
用DECAF_registerOptimizedBlockBeginCallback()注册函数的回调 以检查回调的是否为hook函数 ---> hookapi_insert()：插入记录链.  

- 注册回调函数VMI_REMOVEPROC_CB：removeproc_callback()  

TLB回调 ---> get_new_modules ---> VMI_insert_module() ---> hookapi.cpp:571：check_unresolved_hooks()(此函数完成了hookapi_hook_function_byname()函数的hook函数注册的功能):“Hooking xx::xx at xx”，funcmap_get_pc()检索fnhook_info列表获取PC，有则DECAF_registerOptimizedBlockBeginCallback（）注册函数的回调 以检查回调的是否为hook函数 ---> hookapi_insert()：插入记录链.  

**Monitor命令：do_hookapitests [procname]** ---> do_hookapitests():获取命令参数procname,给全局变量targetname.

####keylogger
**init_plugin()**：mon_cmds、plugin_cleanup处理函数注册

**Monitor命令：taint_sendkey [key]** ---> do_taint_sendkey():制位taint_key_enabled为1，注册按键key回调，后发送按键key，
- DECAF_KEYSTROKE_CB ---> tracing_send_keystroke():按键的taint_mark制位为1，表示设置该按键为污点,”taint keystroke”.

**Monitor命令：enable_keylogger_check [tracefile]** ---> do_enable_keylogger_check()：创建tracefile日志文件，注册回调函数，
- DECAF_READ_TAINTMEM_CB ---> do_read_taint_mem():根据当前eip和cr3，获取模块及进程信息并保存;使用funcmap_get_name_c(),由sys_call_entry_stack[]函数调用栈顶和cr3得到模块名字和函数名字;把回调时的污染地址、EIP、进程名、模块名、函数名等信息写入keylogger_log。
- DECAF_WRITE_TAINTMEM_CB ---> do_write_taint_mem()：同do_read_taint_mem()函数。
- 使用DECAF_registerOptimizedBlockEndCallback()函数注册 ---> do_block_end_cb()：块结尾回调，如果位函数调用指令，则入栈sys_call_entry_stack，调用check_call():如果栈到顶时自动覆盖部分旧栈数据，并入栈：sys_call_ret_stack[]、sys_call_entry_stack[]、cr3_stack[]; 如果为函数返回指令，则出栈调用check_ret():stack_top--,出栈。

**Monitor命令：disable_keylogger_check** ---> do_disable_keylogger_check()：“disable taintmodule check successfully”，注册回调。  

####tracecap



//网卡  
**Monitor命令：taint_nic [0|1]**:设置网络输入是否污染 ---> do_taint_nic()：设置taint_nic_state的值。

**Monitor命令：taint_nic_filter <clear|proto|sport|dport|src|dst> [any value/tcp|udp/port/port/ip addr/ip addr]**:网卡污染过滤 ---> update_nic_filter()：解析命令参数，以设置 结构体变量pkt_filter的不同成员的值。

**Monitor命令：ignore_dns [0|1]**：是否忽略dns包 ---> set_ignore_dns()：设置conf_ignore_dns的值为0或1.

**Monitor命令：filter_tainted_only [0|1]**:是否只跟踪污染指令 ---> set_tainted_only():设置conf_tainted_only的值为0或1.

**Monitor命令：filter_single_thread_only [0|1]**：是否只跟踪首线程的指令 ---> set_single_thread_only()：设置conf_single_thread_only的值为0或1.

**Monitor命令：filter_kernel_tainted [0|1]**：是否跟踪污染的内核指令 --> set_kernel_tainted():设置conf_tracing_kernel_tainted的值为0或1.

//按键污染  
**Monitor命令：taint_sendkey [key]** ---> do_taint_sendkey():设置taint_key_enabled为1,注册回调，发送按键key。
- 注册回调函数DECAF_KEYSTROKE_CB:tracing_send_keystroke()：置位按键回调的taint_mark为1，以设置污点。

//  
**Monitor命令: trace [pid] [filepath]**:在指定文件中 记录一个进程的执行跟踪 ---> do_tracing()：调用VMI_find_process_by_pid_c()查找进程，设置该进程名为tracename，把filepath给tracefile，调用do_tracing_internal（）后，调用trackproc_start（），设置l_iRunning,l_iNumProc等变量，跟踪进程开始 ---> do_tracing_internal():


###decaf初始化
vl.c:main():主要是建立一个虚拟的硬件环境。它通过参数解析，初始化内存，初始化模拟的设备，CPU参数，初始化KVM等 --> DECAF_init()：初始化callback，初始化tainting，初始化DECAF虚拟设备，虚拟机状态保存和恢复的相关函数注册（register_savevm()是qemu提供的 对设备状态保存和恢复函数进行注册）functinmap、hookapi、VMI初始化  
- --> function_map_init()：虚拟机状态进行保存（function_map_save():空）和恢复（function_map_load()：从guest.log文件中恢复状态 --> parse_function():解析guest.log中的记录 -->funcmap_insert_function():插入函数）的函数注册.
- --> init_hookapi():hookapi_record_heads[]数组清零，虚拟机状态进行保存（hookapi_save()）和恢复（hookapi_load()）的函数注册。
- --> shared/vmi.cpp:VMI_init()：注册TLB执行 回调函数：DECAF_TLB_EXEC_CB：block_end_cb()；设置第一次count_out后，每256次TLB回调，便寻找一次guest OS，直到count_out递减到0，停止，操作系统识别失败，注销TLB回调函数。

 - guest OS识别：结构体数组handle_founds_c[]，存放了所支持的 操作系统列表、find()函数地址、init()函数地址，每次识别都分别对列表中的各find()函数进行执行，find()返回1，表示查找成功，并打印出识别出的操作系统名称，且注销TLB回调函数 --> init().  

 - find():shared/linux_vmi.cpp:find_linux():判断该系统是否为linux，方法是核查init_thread_info,init_task.  
 
 - init():shared/linux_vmi.cpp:linux_vmi_init():注册TLB回调函数：DECAF_TLB_EXEC_CB：Linux_tlb_call_back()