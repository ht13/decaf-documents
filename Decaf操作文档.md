##Decaf操作文档
Right-on-Time Virtual Machine Introspection  
Support for Multiple Platforms  
Precise and Lossless Tainting  
Event-driven programming interfaces  
Dynamic instrumentation management  
#####一、编译
1、安装依赖库（ubuntu环境)
```
sudo apt-get build-dep qemu
sudo apt-get install binutils-dev libboost-all-dev libsdl1.2-dev
```
2、下载配置编译
```
svn checkout http://decaf-platform.googlecode.com/svn/trunk/ decaf-platform-read-only
./configure --enable-tcg-taint --enable-tcg-ir-log --enable-vmi
make
```
#####二、创建qemu虚拟机可启动系统映像文件(如.qcow2)
1、由qemu直接创建

2、可由vmdisk (vmware)转换至qcow2格式
```
qemu-img convert win2kpro.vmdk -O qcow win2kpro.img
```
3、也可由vdi (virtualBox)转换至qcow2格式
```
VBoxManage clonehd --format RAW img.vdi img.raw
qemu-img convert -f raw ubuntu.img -O qcow2 ubuntu.qcow
```
#####三、Linux VMI配置
1、编译语义获取的内核模块。  
在路径`[decaf_path]/shared/kernelinfo/procinfo_generic/`下，有源文件procinfo.c和Makefile，复制两文件至guest OS。
```
make
sudo insmod ./procinfo.ko
dmesg
```
即可查看到类似init_task_addr  = 3246178336的地址信息。

2、对于无编译环境的guest linux系统，下载guest linux对应的linux源码
`ftp://ftp.kernel.org/pub/linux/kernel/`  
下载完成，解压linux内核源码，其根目录路径为：`linux_kernel_path`，分别更改两行：
```
make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
```
变为：
```
make -C [linux_kernel_path] M=$(PWD) modules
make -C [linux_kernel_path] M=$(PWD) clean
```
之后同上
```
make
sudo insmod ./procinfo.ko
dmesg
```
3、按上获取到偏移地址信息后，按照procinfo.ini内容格式，更新增加一项系统内核语义提取项，记得`total`的值+1。
`procinfo.ini`路径：  
`[decaf_path]/shared/kernelinfo/procinfo_generic/procinfo.ini`

4、共享库符号信息配置(可选)。
hook库中的函数必须配置此项！新增文件`[strName].ini`在`[decaf_path]/shared/kernelinfo/procinfo_generic/lib_conf/`路径下。  
获取guest库的符号信息及偏移地址:
```
objdump -T [path_of_shared_library_in_guest_OS] | awk '/\.text/ && $6 !~ /\(.*\)/ {printf("%-30s= %d\n",$7,strtonum("0x"$1)}'
```
按照该目录下其他.ini的内容格式，配置[strName].ini文件即可。
#####四、运行decaf
```
cd  [decaf_root_path]
cd  i386-softmmu
./qemu-system-i386 -monitor stdio -m 512 -netdev user,id=mynet -device rtl8139,netdev=mynet “YOUR_IMAGE”
```
#####五、编译、运行plugins
**编译：**
```
cd  [decaf_root_path]/plugins
cd  xxx_plugin
./configure --decaf-path=[decaf_root_path]
make
```

**对于插件tracecap的编译：**  
\#编译llconf
```
cd [decaf_root_path]/shared/llconf
./configure --prefix=`pwd`/install
make
make install
```
\#编译sleuthkit  
\#如果此步编译失败，可下载已编译好的sleuthkit ，解压覆盖sleuthkit目录即可  
\#下载链接：`http://u.163.com/ZUl71Mil`  提取码: `6N1iZ4z1`
```
cd [decaf_root_path]/shared/sleuthkit
Make
```
\#编译tracecap
```
cd [decaf_root_path]/plugins/tracecap
./configure --decaf-path=[decaf_root_path]
make
```

**运行：**
- callbacktests  
\#查看可用monitor命令  
`help`  
\#载入插件  
`load_plugin ../plugins/callbacktests/callbacktests.so`  
\#指定测试进程  
`do_callbacktests hello`  
\#在guest中运行hello进程，查看结果  
\#如果进程结束前没有测试完所有CALLBACKTESTS_TEST项目，再次运行hello进程测试，直到全部callback测试完成。  

- hookapitests  
\#载入插件  
`load_plugin ../plugins/hookapitests/hookapitests.so`  
\#指定测试进程  
`do_hookapitests hello`  
\#在guest中运行hello进程，查看结果  
\#`TLB` ---> `get_new_modules_x86` ---> `VMI_insert_module()` ---> `check_unresolved_hooks():Hooking xx::xx at xx`可完成hook函数的注册功能  
\#显示“NtCreateFile entry”表示hook函数成功  

- keylogger  
\#载入插件  
`load_plugin ../plugins/keylogger/keylogger.so`  
\#指定记录文件  
`enable_keylogger_check log.txt`  
\#打开读指针污染  
`enable_tainting`  
`taint_pointers on off`  
\#发送key  
`taint_sendkey c`  
\#当看到guest系统中出现key：c时，停止按键污染记录  
`disable_keylogger_check`  
\#查看log.txt污点记录文件  

- tracecap  
\#guest系统运行带测试进程  
`load_plugin tracecap/tracecap.so`  
\#使能指令跟踪  
`enable_emulation`  
\#查看进程，CR3是指针，指向该进程的内核空间页表  
`linux_ps`  
\#跟踪进程执行指令，并记录到文件  
`trace [pid] log.trace`  
\#或要跟踪还未执行的程序  
`tracebyname hello  log.trace`  
\#guest中再运行foo程序  
\#键盘输入污点跟踪记录  
`taint_sendkey [key]  
\#停止跟踪，将有了二进制格式的trace文件  
`trace_stop`  
`unload_plugin`  
\#网络污点跟踪，类似以上步骤，但有两处变化  
\#载入插件后，taint_nic 1，设置所有网络输入为污点  
\# instead of giving taint_sendkey, just simply direct the input to the IP address/port of the virtual machine.  
\#假如网络引起EIP被污染，则立即记录所有跟踪数据并退出  
\#main.ini文件配置tracecap  

#####六、